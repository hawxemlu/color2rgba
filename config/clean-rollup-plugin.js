"use strict";

const fs = require("fs");
const path = require("path");

function isNull(target) {
  return target === undefined || target === null;
}

/**
 * @param {String} path 
 * @return {Boolean}
 */
function hasExt(path) {
  const arr = path.split(".");
  return arr.length > 1;
}

/**
 * @param {String[] | String} target
 * @param {String[] | String} external
 * @return {String[]}
 */
function cleanFile(target = [], external = []) {
  const cleaned = [];
  for (const targetPath of target) {
    const normalisedPath = path.normalize(targetPath);
    // 路径是否存在
    if (fs.existsSync(normalisedPath)) {
      // 路径为文件夹
      if (!hasExt(normalisedPath)) {
        const paths = fs.readdirSync(normalisedPath).map(p => path.join(normalisedPath, p));
        const sub = cleanFile(paths, external);
        cleaned.push(...sub, normalisedPath);
        fs.rmdirSync(normalisedPath);
        continue;
      }
      // 路径为文件
      cleaned.push(normalisedPath);
      fs.rmSync(normalisedPath);
    }
  }
  return cleaned;
}

/**
 * @param {Object} options 
 * @param {String[] | String} options.target
 * @param {String[] | String} options.external
 */
function cleanRollupPlugin({ target = [], external = []} = {}) {
  return {
    name: 'cleaner',
    buildStart(options) {
      const start = +new Date();
      target = !isNull(target) ? target : [];
      external = !isNull(external) ? external : [];
      if (typeof target === "string") {
        target = [target];
      }
      if (typeof external === "string") {
        external = [external];
      }
      const cleaned = cleanFile(target, external);
      const end = +new Date();
      if (cleaned.length > 0) {
        console.info(`clean [\n  ${cleaned.join(",\n  ")}\n] in ${end - start}ms`);
      }
    }
  }
}

module.exports = cleanRollupPlugin;

if (require.main === module) {
  cleanRollupPlugin({
    target: ["build"]
  }).buildStart()
}