const { terser } = require("rollup-plugin-terser");
const commonConfig = require("./rollup.common.js");

const prodPlugins = [
  terser()
];

const prodConfig = { ...commonConfig };
prodConfig.output.forEach((item) => item.sourcemap = false);
prodConfig.plugins.push(...prodPlugins);

module.exports = prodConfig;
