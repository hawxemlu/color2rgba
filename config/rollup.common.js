const resolve = require("@rollup/plugin-node-resolve");
const babel = require("@rollup/plugin-babel");
const json = require("@rollup/plugin-json");
const cleanRollupPlugin = require("./clean-rollup-plugin");

module.exports =  {
  input: "src/main.js",
  output: [{
    name: "color2rgba",
    file: "build/main.min.js",
    format: "umd",
    sourcemap: true,
  }, {
    file: "build/main.esm.min.js",
    format: "esm",
    sourcemap: true,
  }],
  plugins: [
    cleanRollupPlugin({
      target: ["build"]
    }),
    resolve({
      moduleDirectories: ["node_modules"]
    }),
    /* 
     * https://github.com/rollup/plugins/tree/master/packages/babel#babelhelpers 
     * 如果仅作为单独模块使用, 可以设置babelHelpers为runtime, 并引入@babel/runtime,@babel/plugin-transform-runtime
     * 此时babel的辅助函数会单独打包, 需设置external: [/@babel\/runtime/]
     * 使用时需额外引入babel辅助函数
     */
    babel({ babelHelpers: "bundled" }), 
    json()
  ],
  external: []
}
