# color2rgba

> color字符串转换为rgba字符串，将输入的颜色字符串及透明度值拼合成rgba颜色值
>
> 使用场景：低代码平台颜色值输入



# 示例

```javascript
/**
 * @param {String} str 颜色字符串,支持rgb(),rgba(),8位、6位、3位16进制颜色和标准定义的146个颜色名
 * @param {Number?} opacity 透明度, 默认1, 如传入大于1的数字时将被认作百分比处理
 * @param {Boolean?} reverse 是否反色输出, 仅反转rgb值
 * @returns {String?} rgba颜色字符串 或 null(传入的颜色不合规时)
 */
color2rgba('#42f') //rgba(68,34,255,1)
color2rgba('#42f', 0.7) //rgba(68,34,255,0.7)
color2rgba('#42f', 0.7, true) //rgba(187,221,0,0.7)

color2rgba('#cefa') //rgba(204,238,255,0.67)
color2rgba('#cefa', 43) //rgba(204,238,255,0.43)
color2rgba('#cefa', 43, true) //rgba(51,17,0,0.43)

color2rgba('#412366') //rgba(65,35,102,1)
color2rgba('#412366', 0.8) //rgba(65,35,102,0.8)
color2rgba('#412366', 0.8, true) //rgba(190,220,153,0.8)

color2rgba('#13632264') //rgba(19,99,34,0.39)
color2rgba('#13632264', 0.5) //rgba(19,99,34,0.5)
color2rgba('#13632264', 0.5, true) //rgba(236,156,221,0.5)

color2rgba('rgb(22,33,44)') // rgba(22,33,44,1)
color2rgba('rgb(22,33,44)', 90) //rgba(22,33,44,0.9)
color2rgba('rgb(22,33,44)', 90, true) //rgba(233,222,211,0.9)

color2rgba('rgba(55,132,88,0.33)') //rgba(55,132,88,0.33)
color2rgba('rgba(55,132,88,0.33)', 65) //rgba(55,132,88,0.65)
color2rgba('rgba(55,132,88,0.33)', 65, true) //rgba(200,123,167,0.65)

color2rgba('wheat') //rgba(245,222,179,1)
color2rgba('wheat', 0.8) //rgba(245,222,179,0.8)
color2rgba('wheat', 0.8, true) //rgba(10,33,76,0.8)
```
