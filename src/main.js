import colors from './colors';

/**
 * @param {String} str 颜色字符串,支持rgb(),rgba(),8位、6位、3位16进制颜色和标准定义的146个颜色名
 * @param {Number?} opacity 透明度, 默认1, 如传入大于1的数字时将被认作百分比处理
 * @param {Boolean?} reverse 是否反色输出, 仅反转rgb值
 * @returns {String?} rgba颜色字符串 或 null(传入的颜色不合规时)
 */
function color2rgba(str, opacity, reverse = false) {
  try {
    // 如果没有传opacity, 且字符串中包含alpha值时使用原alpha值
    let flag = (opacity === undefined || opacity === null) ? true : false;
    opacity = flag ? 1 : opacity;
    let midStr = str.replace(/[\s]/g, '').toLowerCase();
    if (colors[midStr]) {
      midStr = colors[midStr];
    }
    let r, g, b, a = opacity <= 1 ? opacity : (opacity / 100);
    if (/rgb\([\d]{1,3},[\d]{1,3},[\d]{1,3}\)/i.test(midStr)) {
      let rgb = /rgb\([\d]{1,3},[\d]{1,3},[\d]{1,3}\)/i.exec(midStr)[0];
      rgb = /rgb\((.+)\)/i.exec(rgb)[1];
      [r, g, b] = rgb.split(',');
      return !reverse ? `rgba(${r},${g},${b},${a})` : `rgba(${255 - r},${255 - g},${255 - b},${a})`;
    }
    if (/rgba\([\d]{1,3},[\d]{1,3},[\d]{1,3},(.+)\)/i.test(midStr)) {
      let rgba = /rgba\([\d]{1,3},[\d]{1,3},[\d]{1,3},(.+)\)/i.exec(midStr)[0];
      rgba = /rgba\((.+)\)/i.exec(rgba)[1];
      if (flag) {
        [r, g, b, a] = rgba.split(',');
      } else {
        [r, g, b] = rgba.split(',');
      }
      return !reverse ? `rgba(${r},${g},${b},${a})` : `rgba(${255 - r},${255 - g},${255 - b},${a})`;
    }
    if (/#[A-Za-z0-9]{3,8}/i.test(midStr)) {
      let hex = /#[A-Za-z0-9]{3,8}/i.exec(midStr)[0];
      hex = hex.slice(1).split('');
      if (hex.length >= 6) {
        const [r1, r2, g1, g2, b1, b2, a1, a2] = hex;
        r = parseInt(`${r1}${r2}`, 16);
        g = parseInt(`${g1}${g2}`, 16);
        b = parseInt(`${b1}${b2}`, 16);
        if (flag && a1 !== undefined && a2 !== undefined) {
          a = (parseInt(`${a1}${a2}`, 16) / 255).toFixed(2);
        }
        return !reverse ? `rgba(${r},${g},${b},${a})` : `rgba(${255 - r},${255 - g},${255 - b},${a})`;
      }
      if (hex.length >= 3) {
        const [r1, g1, b1, a1] = hex;
        r = parseInt(`${r1}${r1}`, 16);
        g = parseInt(`${g1}${g1}`, 16);
        b = parseInt(`${b1}${b1}`, 16);
        if (flag && a1 !== undefined) {
          a = (parseInt(`${a1}${a1}`, 16) / 255).toFixed(2);
        }
        return !reverse ? `rgba(${r},${g},${b},${a})` : `rgba(${255 - r},${255 - g},${255 - b},${a})`;
      }
    }
  } catch (e) {}
  return null;
}

export default color2rgba;